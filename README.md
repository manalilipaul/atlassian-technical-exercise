Automated Process:

Using Cloudformation - stacksets 

IAM : Profile and Roles for services

Security Groups: Inbound and Outbound for EC2 and RDS

Network: Route53 > Internet Gateway > VPC Router > ACL1/ACL2 > VPC > Private Subnet1/Subnet2 > Public Subnet1/Subnet2 

Loadbalancer : ELB

Compute: ASG > EC2

Database : RDS

Storage : S3 Backup and Logging
								
Files:

	master.yaml - master stack

infra/

	iam.yaml - iam stack 
	vpc.yaml - vpc stack 
	s3.yaml - s3 stack 
	securitygroup.yaml - securitygroup stack
	rds.yaml - rds mysql stack
	asg-jira.yaml - asg jira stack
	elb.yaml - elb stack
	route53.yaml - route53 stack

Issues Encounterd:
	set jira on userdata the jira dns, still ongoing.
	set jira on userdata to connect to rds dns, still ongoing.
    

Manual Process:

Network: 
	create vpc with private and public subnet using nat gateway.
	
Compute: 
	create launch tempalate  > userdata.
	create elb.
	create asg > use launch template >use elb.
	
Database: 
	create parameter group for mysql 5.7 Jira Atlassian
	create RDS - mysql5.7

Storage:
	S3 bucket for backups
	s3 bucket for logs

Route53:
	route elb to jira.paulmanalili.site
	route rds to rds.paulmanalili.site

Import Data: 
	Files: activeobjects.xml and entities.xml
	upload to Jira Server to /var/atlassian-jira-home/import/ 
	In Jira Settings > System > Import & Export > Restore System

entities.xml, it seems after loading this my admin account got revoked, and i need to use recovery_admin.

Issues Encounterd:
	MySQL5.7 connector Jar
	MySQL5.7 parameters needs to set
	Certifcate from Atlassian needs to be updated once I load the entities.xml
	Use recovery_admin to reover lost admin account
    

Changes:

Changed Logo and Header color

userdata:
	
	
	#!/bin/bash
	sudo su

	#update dependencies
	yum update -y

	#Install Java
	sudo yum install java-1.8.0 -y

	#download mysql db connector
	cd /tmp
	wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.48.tar.gz	
	tar -xvf mysql-connector-java-5.1.48.tar.gz
	rm -rf mysql-connector-java-5.1.48.tar.gz

	#Install Jira
	mkdir /opt/atlassian-jira /var/atlassian-jira-home
	cd /opt/atlassian-jira
	sudo wget https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-8.13.0.tar.gz
	tar xzf /opt/atlassian-jira/atlassian-jira-software-8.13.0.tar.gz
	rm -f /opt/atlassian-jira/atlassian-jira-software-8.13.0.tar.gz
	mv /opt/atlassian-jira/atlassian-jira-software-8.13.0-standalone /opt/atlassian-jira/jira-8.13.0
	
	#copy mysql-connector
	cp /tmp/mysql-connector-java-5.1.48/mysql-connector-java-5.1.48-bin.jar .

	sed -i 's+jira.home =+jira.home = /var/atlassian-jira-home+g' /opt/atlassian-jira/jira-8.13.0/atlassian-jira/WEB-INF/classes/jira-application.properties

	# Backup Jira Data
	echo '# Cron Upload' > /tmp/mycron
	echo '*/5 * * * * /usr/bin/aws s3 sync --sse AES256 /var/atlassian-jira-home arn:aws:s3:::s3backup-atlassian-technical-exam/ > /var/log/jirasync.log 2>&1' >> /tmp/mycron
	/usr/bin/crontab /tmp/mycron
	rm -rf /tmp/mycron

	useradd jira
	chown -R jira:jira /opt/atlassian-jira
	chown -R jira:jira /var/atlassian-jira-home

	su jira

	/opt/atlassian-jira/jira-8.13.0/bin/start-jira.sh